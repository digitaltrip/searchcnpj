require('dotenv/config');

(async function() {

  const mongoConfig = require("./common/connection/connectionMongo");
  const mongoose    = require('mongoose');

  const app         = require("./common/connection/connectionExpress");
  const branchRoute = require("./common/routes");
  const PORT        = 8080;

  mongoose.Promise = global.Promise;
  mongoose.connect(mongoConfig.url, { useNewUrlParser: true , useUnifiedTopology: true})
      .then(() => { console.log("Mongo conectado com Sucesso"); })
      .catch(err => {
          console.log("Não foi possivel conectar no Mongo, Erro: " + err);
          process.exit();
      });

  branchRoute(app);

  app.listen(PORT, function() {

    console.log("Servidor rodando na porta " + PORT);

  });
  
  
})().catch(console.err);