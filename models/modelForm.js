const mongoose = require('mongoose');

const CNPJDestinySchema = mongoose.Schema({
        _id: { type: mongoose.Schema.Types.ObjectId , required: true },
        cnpjForm:{
                id: { type: Number, require: true },
                cnpj:{ type: String, require: true },
                nome:{ type: String, require: true },
                logradouro:{ type: String, require: true },
                numero:{ type: Number, require: true },
                complemento:{ type: String, require: true },
                municipio:{ type: String, require: true },
                uf:{ type: String, require: true },
                cep:{ type: String, require: true },
                telefone:{ type: String, require: true },
                email:{ type: String, require: true }
              }
    });

module.exports = mongoose.model('cnpjForm', CNPJDestinySchema);