const mongoose = require('mongoose');
const CnpjForm  = require('../models/modelForm.js');

exports.create = (request, response) => {
    const cnpjForm = new CnpjForm({
        _id: new mongoose.Types.ObjectId(),
        cnpjForm:{
            id:request.body.id,
            cnpj:request.body.cnpj,
            nome:request.body.nome,
            logradouro:request.body.logradouro,
            numero:request.body.numero,
            complemento:request.body.complemento,
            municipio:request.body.municipio,
            uf:request.body.uf,
            cep:request.body.cep,
            telefone:request.body.telefone,
            email:request.body.email
        }
    });

    cnpjForm.save()
        .then(data => {
            return response
                .status(201)
                .send(data);
        })
        .catch(err => {
            return response
                .status(500)
                .send({ message: err.message });
        });
}

exports.findCNPJ = (request, response) => {
    CnpjForm.findOne({"cnpjForm.cnpj":request.params.cnpj})
        .then(result => {
            if(!result){
                return response
                    .status(404)
                    .send({ message: `Nenhum CNPJ foi encontrado` })
            }

            return response
                .status(200)
                .send(result);
        })
        .catch(err => {
            response
                .status(500)
                .send({
                    message: err.message || "Erro ao consultar o CNPJ."
                });
        });
}

exports.findAll = (request, response) => {
    CnpjForm.find()
        .then(result => {
            if (result.length === 0) {
                return response
                    .status(404)
                    .send({ message: `Nenhum CNPJ foi encontrado` })
            }

            return response
                .status(200)
                .send(result);
        })
        .catch(err => {
            return response
                .status(500)
                .send({
                    message: err.message || "Erro ao consultar o CNPJ."
                });
        });
}

exports.findOne = (request, response) => {
    CnpjForm.findById(request.params.id)
    .then(result => {
        if (!result) {
            return response
                .status(404)
                .send({ message: "CNPJ não foi encontrado" 
              });
        }

        return response
            .send(result)
            .status(200);
    })
    .catch(err => {
        if (err.kind === 'ObjectId') {
            return response
                .status(404)
                .send({
                    message: "CNPJ não foi encontrado"
                });
        }

        return response
            .status(500)
            .send({
                message: "Erro ao recuperar o CNPJ"
            });
    });
}

exports.update = (request, response) => {
    if (!request.body) {
        return response
            .status(400)
            .send({ message: `Informe os dados do hotel` });
    }

    CnpjForm.findOneAndUpdate(request.params.id, {
        cnpjForm:{
            cnpj:request.body.cnpj,
            nome:request.body.nome,
            logradouro:request.body.logradouro,
            numero:request.body.numero,
            complemento:request.body.complemento,
            municipio:request.body.municipio,
            uf:request.body.uf,
            cep:request.body.cep,
            telefone:request.body.telefone,
            email:request.body.email
          }
        }, { new: true })
        .then(cnpjForm => {
            if (!cnpjForm) {
                return response
                    .status(404)
                    .send({
                        message: `O CNPJ com id: ${request.params.id} não foi encontrado`
                    });
            }

            return response
                .send(cnpjForm)
                .status(200);
        })
        .catch(err => {
            if (err.kind === 'ObjectId') {
                return response
                    .status(404)
                    .send({
                        message: `O CNPJ com id: ${request.params.id} não foi encontrado`
                    });
            }

            return response
                .status(500)
                .send({ message: `Erro ao atualizar o CNPJ com id: ${request.params.id}` });
        });
}

exports.delete = (request, response) => {
    CnpjForm.findByIdAndDelete(request.params.id)
      .then(cnpjForm => {
          if (!cnpjForm) {
              return response
                    .status(404)
                    .send({ message: `O CNPJ com id: ${request.params.id} não foi encontrado` });
          }

          return response
                .status(204)
                .send({message: `CNPJ removido com sucesso!`});

      })
      .catch(err => {
          if (err.kind === 'ObjectId') {
              return response
                  .status(404)
                  .send({
                      message: `O CNPJ com id: ${request.params.id} não foi encontrado`
                  });
          }

          return response
              .status(500)
              .send({ message: `Erro ao remover o CNPJ com id: ${request.params.id}` });
      });
}