module.exports = (app) => {
    const formController = require ('../controllers/formController');  
    //Criar
    app.post("/create",formController.create);
    //Buscar Todos cpf
    app.get("/findall",formController.findAll);
    //Buscar um id
    app.get("/findone/:id",formController.findOne);
    //Buscar um cpf
    app.get("/findcnpj/:cnpj",formController.findCNPJ);
    //Alterar 
    app.post("/alter/:id",formController.update);
    //Deletar
    app.get("/remove/:id",formController.delete);
};