const express    = require('express');

var cors         = require('cors')

const app        = express();
const routes     = require('../routes');
const bodyParser = require('body-parser');

app.use(cors())
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
routes(app);

app.get('/health', function(req,res) {
    res.json({"status": "UP"})
    .status(200);
});

module.exports = app;